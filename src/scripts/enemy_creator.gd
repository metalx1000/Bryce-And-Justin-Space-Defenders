extends Node2D

var enemy = preload("res://enemy.tscn")


func _ready():
	$enemy_creator_ani.play("move")
	
	pass


func _on_Timer_timeout():
	var current_scene = get_tree().get_current_scene()
	if rand_range(0,3) > 2:
		var e = enemy.instance()
		e.position = position
		current_scene.add_child(e)
