extends Node2D

var speed = 500
onready var sprite = get_node("Sprite")
var blue = preload("res://assets/sprites/laser_01.tres")
var red = preload("res://assets/sprites/laser_02.tres")
var green = preload("res://assets/sprites/laser_03.tres")
var color = "blue"

func _ready():
	if color == "red":
		$Sprite.set_texture(red)
	elif color == "green":
		$Sprite.set_texture(green)
	

func _process(delta):
	position.x += speed * delta

