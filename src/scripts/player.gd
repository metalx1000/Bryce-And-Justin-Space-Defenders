extends Node2D
var justin_sprite = preload("res://assets/sprites/justin.tres")
var bryce_sprite = preload("res://assets/sprites/bryce.tres")

var laser = preload("res://laser.tscn")
var laser_color = preload("res://assets/sprites/laser_02.tres")

var speed = 500

func _ready():
	if Global.player == "bryce":
		$Sprite.set_texture(bryce_sprite)
	else:
		$Sprite.set_texture(justin_sprite)

func _process(delta):
	if Input.is_action_pressed("ui_up"):
		position.y -= speed * delta
		
	if Input.is_action_pressed("ui_down"):
		position.y += speed * delta

	if Input.is_action_just_pressed("shoot"):
		shoot()
		
	if position.y < 10:
		position.y = 10
	
	if position.y > get_viewport_rect().size.y:
		position.y = get_viewport_rect().size.y

func shoot():
		var current_scene = get_tree().get_current_scene()
		var l = laser.instance()
		l.position = position
		if Global.player == "bryce":
			l.color = "red"
		current_scene.add_child(l)
