extends Node2D


var speed = 500.0
var laser = preload("res://laser.tscn")
var laser_color = preload("res://assets/sprites/laser_02.tres")

func _ready():
	randomize()
	speed = rand_range(350,500)
	$Timer.wait_time = 1

func _process(delta):
	position.x -= speed * delta


func shoot():
	var current_scene = get_tree().get_current_scene()
	var l = laser.instance()
	l.position = position
	l.speed = -600
	l.color = "green"
	current_scene.add_child(l)
