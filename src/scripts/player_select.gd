extends Node2D

func _ready():
	$intro_ani.play("intro")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_left"):
		$justin.grab_focus()
	
	if Input.is_action_just_pressed("ui_right"):
		$bryce.grab_focus()
	
	$justin.pressed


func _on_justin_mouse_entered():
	$sound.play()


func _on_bryce_mouse_entered():
	$sound.play()


func _on_justin_pressed():
	$select.play()
	Global.player = "justin"
	$intro_ani.play("fade_out")


func _on_bryce_pressed():
	$select.play()
	Global.player = "bryce"
	$intro_ani.play("fade_out")

func next():
	get_tree().change_scene("res://game.tscn")
